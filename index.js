// setting varibles

var in1 = document.getElementById("first");
var in2 = document.getElementById("second");
var ans = document.getElementById("answer");
var add = document.getElementById("add");
var sub = document.getElementById("sub");
var mul = document.getElementById("mul");
var div = document.getElementById("div");
var log = document.getElementById("log");
var sqrt = document.getElementById("sqrt");
var sq = document.getElementById("sq");
var sin = document.getElementById("sin");
var cos = document.getElementById("cos");
var tan = document.getElementById("tan");

var reset = document.getElementById("reset");
var op = document.getElementById("mid");
var f = document.getElementById("features");
var h7 = document.getElementById("h7");
var hides = document.getElementById("hides");

var output="NaN";

// function for reseting input

reset.addEventListener("click",function(){
	in1.value="";in2.value="";ans.value="";
	in2.setAttribute("type", "");
	h7.style.display = '';
	hides.style.display="none";
});


f.addEventListener("click",function(){
	in2.setAttribute("type", "hidden");
	h7.style.display = 'none';
	hides.style.display='block';

});

// function for reseting input

add.addEventListener("click",function(){ 
	if(isNaN(output)){
	alert("Invalid values");
	}
	 output=parseInt(in1.value)+parseInt(in2.value); //parseInt converting the string text into integer
	console.log(output);
	ans.value = output;
});

// function for reseting input

mul.addEventListener("click",function(){
	if(isNaN(output)){
	alert("Invalid values");
	}
	 output=parseInt(in1.value)*parseInt(in2.value);
	console.log(output);
	ans.value = output;
});

// function for reseting input

sub.addEventListener("click",function(){
	if(isNaN(output)){
	alert("Invalid values");
	}
 output=parseInt(in1.value)-parseInt(in2.value);
	console.log(output);
	ans.value = output;
});

// function for reseting input
div.addEventListener("click",function(){
	if(isNaN(output)){
	alert("Invalid values");
	}
	 output=parseInt(in1.value)/parseInt(in2.value);
	console.log(output);
	ans.value = output;

});


div.addEventListener("click",function(){
	if(isNaN(output)){
	alert("Invalid values");
	}

	 output=parseInt(in1.value)/parseInt(in2.value);
	console.log(output);
	ans.value = output;

});

log.addEventListener("click",function(){
	if(isNaN(output)){
	alert("Invalid values");
	}

	 output=Math.log(parseInt((in1.value)));
	console.log(output);
	ans.value = output;

});

sin.addEventListener("click",function(){
	if(isNaN(output)){
	alert("Invalid values");
	}

 output=Math.sin(parseInt(in1.value));
	console.log(output);
	ans.value = output;

});

cos.addEventListener("click",function(){
	if(isNaN(output)){
	alert("Invalid values");
	}

	 output=Math.cos(parseInt(in1.value));
	console.log(output);
	ans.value = output;

});

tan.addEventListener("click",function(){
	if(isNaN(output)){
	alert("Invalid values");
	}

	 output=Math.tan(parseInt(in1.value));
	console.log(output);
	ans.value = output;

});

sqrt.addEventListener("click",function(){
	if(isNaN(output)){
	alert("Invalid values");
	}

	 output=Math.sqrt(parseInt(in1.value));
	console.log(output);
	ans.value = output;

});

sq.addEventListener("click",function(){
	if(isNaN(output)){
	alert("Invalid values");
	}
	output=parseInt(in1.value)*parseInt(in1.value);
	console.log(output);
	ans.value = output;


});


// clock script

function startTime() {

  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  var c=document.getElementById("time");
  m = checkTime(m);
  s = checkTime(s);
  c.innerHTML =	h + ":" + m + ":" + s;
  setTimeout(startTime, 500);

}

function checkTime(i) {
  if (i < 10) {i = "0" + i};  
  return i;
}

// script for google map api
function initMap() {
  var myLatLng = {lat:28.7041,lng:77.1025};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Hello World!'
  });
	}